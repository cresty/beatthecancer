﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLoadVariable : MonoBehaviour
{

    public string key1;
    public string key2;
    public string currentKey;
    public static Language lang;// = Language.English;
    public Toggle t1;
    public Toggle t2;

    /*var medium: Toggle;
    var hard: Toggle;
    var hardcore: Toggle;
    var strat1 : Toggle;
    var strat2 : Toggle;
    var strat3 : Toggle;
    var strat4 : Toggle;*/

    // Use this for initialization
    void Start()
    {
        currentKey = key1;
    }

    // Update is called once per frame
    void Update()
    {
        if (t1.isOn)
            currentKey = key1;
        if (t2.isOn)
            currentKey = key2;

        LanguageManager languageManager = LanguageManager.Load();
        GetComponent<Text>().text = languageManager.GetString(currentKey);

    }

    void setKeyOne(string key)
    {
        currentKey = key1;
    }

    void setKeyTwo(string key)
    {
        currentKey = key2;
    }
    
}
