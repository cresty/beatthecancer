﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Test : MonoBehaviour, IPointerClickHandler{

    public Language language;
    public LanguageManager languageManager;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        languageManager = new LanguageManager();
        LanguageManager.currentLanguage = language;
        languageManager.setLanguage(language);
        languageManager = LanguageManager.Load();
        Debug.Log("it works");
    }
}
