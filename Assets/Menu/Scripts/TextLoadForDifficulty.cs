﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLoadForDifficulty : MonoBehaviour
{

    public string key1;
    public string key2;
    public string key3;
    public string key4;
    public string key5;
    public string key6;
    public string key7;
    public string key8;
    public string key9;
    public string currentKey;
    public static Language lang;// = Language.English;
    public Toggle t1;
    public Toggle t2;
    public Toggle t3;
    public Toggle t4;
    public Toggle t5;
    public Toggle t6;

    /*var medium: Toggle;
    var hard: Toggle;
    var hardcore: Toggle;
    var strat1 : Toggle;
    var strat2 : Toggle;
    var strat3 : Toggle;
    var strat4 : Toggle;*/

    // Use this for initialization
    void Start()
    {
        currentKey = key1;
    }

    // Update is called once per frame
    void Update()
    {
        if (t1.isOn && t4.isOn)
            currentKey = key1;
        else if (t1.isOn && t5.isOn)
            currentKey = key2;
        else if (t1.isOn && t6.isOn)
            currentKey = key3;
        else if (t2.isOn && t4.isOn)
            currentKey = key4;
        else if (t2.isOn && t5.isOn)
            currentKey = key5;
        else if (t2.isOn && t6.isOn)
            currentKey = key6;
        else if (t3.isOn && t4.isOn)
            currentKey = key7;
        else if (t3.isOn && t5.isOn)
            currentKey = key8;
        else if (t3.isOn && t6.isOn)
            currentKey = key9;

        LanguageManager languageManager = LanguageManager.Load();
        GetComponent<Text>().text = languageManager.GetString(currentKey);

    }
}
