﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

public class Translation
{
    [XmlAttribute("Key")]
    public string key;
    
    [XmlAttribute("English")]
    public string English;

    [XmlAttribute("Spanish")]
    public string Spanish;

    [XmlAttribute("German")]
    public string German;

    public Translation()
    {
        key = "New Key";
        English = string.Empty;
        Spanish = string.Empty;
        German = string.Empty;
    }

    public string GetValue(Language language)
    {
        switch (language)
        {
            case Language.English:
                return English;
            case Language.Spanish:
                return Spanish;
            case Language.German:
                return German;
            default:
                return string.Empty;
        }
    }


}

