﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;

[XmlRoot("LanguageManager")]
public class LanguageManager{

    #if UNITY_EDITOR
    private static string g_filePath = Path.Combine(Application.streamingAssetsPath, "Xml/Languages.xml");
#else
    private static string g_filePath = Path.Combine(Application.streamingAssetsPath, "Xml/Languages.xml");
#endif


    [XmlEnum("CurrentLenguage")]
    public Language defaultLanguage;
    public static Language currentLanguage = Language.English;

    [XmlArray("Translations"), XmlArrayItem("Translations")]
    public List<Translation> translations;

    public bool ContainsKey(string key)
    {
        return FindEntry(key) >= 0;
    }

    public string getCurrentLanguage()
    {
        return currentLanguage.ToString();
    }

    public LanguageManager()
    {
        defaultLanguage = currentLanguage;
        translations = new List<Translation>();
        //Debug.Log(g_filePath);
    }
    

    public void setLanguage (Language lang)
    {
        currentLanguage = lang;
        translations = new List<Translation>();
    }

    public string GetString(string key)
    {
        int i = FindEntry(key);
        if (i >= 0)
            return translations[i].GetValue(currentLanguage);

        return string.Empty;
    }

    public void Add(Translation translation)
    {
        Insert(translation, true);
    }

    public void UpdateTranslation (Translation translation)
    {
        Insert(translation, false);
    }

    public void Remove (Translation translation)
    {
        translations.Remove(translation);
    }

    public void Remove(int index)
    {
        translations.RemoveAt(index);
    }

    public void Remove(string key)
    {
        int i = FindEntry(key);

        if (i >= 0)
            Remove(i);
    }

    private void Insert(Translation translation, bool add)
    {
        int i = FindEntry(translation.key);

        if (i>= 0)
        {
            if (add)
                return;

            translations[i] = translation;
        }

        if (add)
        {
            translations.Add(translation);
        }
    }

    public string GetString(string key, Language language)
    {
        int i = FindEntry(key);
        if (i >= 0)
            return translations[i].GetValue(language);

        return string.Empty;
    }

    private int FindEntry(string key)
    {
        for (int i = 0; i < translations.Count; i++)
        {
            if (translations[i].key.Equals(key))
                return i;
        }
        return -1;
    }

    public void Save()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(LanguageManager));
        using (FileStream stream = new FileStream(g_filePath, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public static LanguageManager Load()
    {
        if (File.Exists(g_filePath))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(LanguageManager));
            using (FileStream stream = new FileStream(g_filePath, FileMode.Open))
            {
                return serializer.Deserialize(stream) as LanguageManager;
            }
        }
        return new LanguageManager();
    }
}



public enum Language
{
    English,
    Spanish,
    German
}
