﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLoad : MonoBehaviour {

    public string key;
    public static string keyAux;
    public static Language lang;// = Language.English;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (keyAux != null)
            key = keyAux;
        LanguageManager languageManager = LanguageManager.Load();
        GetComponent<Text>().text = languageManager.GetString(key);
		
	}

    public static void setKey (string keyVar)
    {
        keyAux = keyVar;
    }

    /*public void setLang(Language language)
    {
        lang = language;
    }*/
}
