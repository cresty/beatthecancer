﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CellReplication : MonoBehaviour {

    public int life = 1;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public abstract IEnumerator WaitAndDuplicate();
    
    public virtual void Increment()
    {
        GameController.maxSpawn++;
        GameController.cancerAmount++;
    }

    public virtual void Decrement()
    {
        GameController.maxSpawn--;
        GameController.cancerAmount--;
    }

    public virtual void DecrementTutorial()
    {
        TutorialController.maxSpawn--;
        TutorialController.cancerAmount--;
    }

    public virtual IEnumerator LoseLife()
    {
        yield return new WaitForSeconds(GameController.deathTime);
        life--;
        if (life == 0 && gameObject != null)
        {
            Destroy(gameObject);
            Decrement();
        }
    }

    public virtual IEnumerator LoseLifeTutorial()
    {
        yield return new WaitForSeconds(GameController.deathTime);
        life--;
        if (life == 0 && gameObject != null)
        {
            Destroy(gameObject);
            DecrementTutorial();
        }
    }

}
