﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour {

    [HideInInspector] public static int maxSpawn = 0;
    [HideInInspector] public static int max = 100;
    [HideInInspector] public static GameObject[] Prefabs;
    [HideInInspector] public static float radio = 1f;
    [HideInInspector] public static float time;
    [HideInInspector] public static float startTime;
    [HideInInspector] public static float deathTime;
    [HideInInspector] public static int healthyDeath;
    [HideInInspector] public static int cancerDeath;
    [HideInInspector] public static int healthyMutatesToCancer;
    [HideInInspector] public static int cancerMutatesToStem;
    [HideInInspector] public static int cancerMutatesToFast;
    [HideInInspector] public static int healthyStemReplication;
    [HideInInspector] public static int cancerStemReplication;
    [HideInInspector] public static int cancerFastReplication;
    [HideInInspector] public static int healthyResistance;
    [HideInInspector] public static int healthyStemResistance;
    [HideInInspector] public static int cancerResistance;
    [HideInInspector] public static int cancerStemResistance;
    [HideInInspector] public static int cancerFastResistance;
    [HideInInspector] public static int healthyNum;
    [HideInInspector] public static int healthyStemNum;
    [HideInInspector] public static int cancerStemNum;
    [HideInInspector] public static int cancerFastNum;
    [HideInInspector] public static int cancerNum;
    [HideInInspector] public static int healthyAmount;
    [HideInInspector] public static int cancerAmount;
	public Vector3 randomOffset;
    public bool isPause = false;
    public GameObject pauseMenu;
    public Slider progress;

    // Use this for initialization
    void Start () {

        startTime = 2.0f;
        Prefabs = Resources.LoadAll<GameObject>("Prefabs") as GameObject[];
        for (int i = 0; i < healthyNum; i++)               // Celulas Sanas
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[1], (randomOffset), Quaternion.identity);
        }
        for (int i = 0; i < healthyStemNum; i++)    //Celulas Madre Sanas
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[4], (randomOffset), Quaternion.identity);
            healthyAmount++;
        }
        for (int i = 0; i < cancerNum; i++)              // Celulas Cancer
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[0], (randomOffset), Quaternion.identity);
            cancerAmount++;
        }
        for (int i = 0; i < cancerStemNum; i++)         // Celulas Cancer Madre
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[2], (randomOffset), Quaternion.identity);
            cancerAmount++;
        }
        for (int i = 0; i < cancerFastNum; i++)        // Celulas Madre Rapida
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[3], (randomOffset), Quaternion.identity);
            cancerAmount++;
        }
        setCountText();
    }

    void setCountText()
    {
        progress.maxValue = healthyAmount + cancerAmount;
        progress.value = healthyAmount;
    }

    static public void Init (int healthyDeathVar, int cancerDeathVar,int healthyMutatesToCancerVar, int cancerMutatesToStemVar, 
        int cancerMutatesToFastVar, int healthyStemReplicationVar, int cancerStemReplicationVar, int cancerFastReplicationVar, 
        float timeVar, float deathTimeVar, int healthyNumVar, int healthyStemNumVar, int cancerNumVar, int cancerStemNumVar, 
        int cancerFastNumVar, int healthyResistanceVar, int healthyStemResistanceVar, int cancerResistanceVar, int cancerStemResistanceVar, 
        int cancerFastResistanceVar)
    {
        /*
        * 0 Healthy Cell Dies
        * 1 Cancer Cell Dies
        * 2 Healthy Cell Mutates
        * 3 Cancer Cell Mutates into Cancer Stem Cell
        * 4 Cancer Cell Mutates en Fast Cancer Cell
        * 5 Healthy Stem Cell Replicates
        * 6 Cancer Stem Cell Replicates
        * 7 Fast Cancer Cell Replicates
        * 8 Time
        * 9 Delay for Cell Death
        * 19 Initial Healthy Cells
        * 11 Initial Healthy Stem Cells
        * 12 Initial Cancer Cells
        * 13 Initial Cancer Stem Cells
        * 14 Initial Fast Cancer Cells
        * 15 Healthy Cells Increase Resistance
        * 16 Healthy Stem Cells Increase Resistance
        * 17 Cancer Cells Increase Resistance
        * 18 Cancer Stem Cells Increase Resistance
        * 19 Fast Cancer Cells Increase Resistance
        */
        //Prefabs = Resources.LoadAll("Prefabs", GameObject);
        healthyAmount = 0;
        cancerAmount = 0;
        healthyDeath = healthyDeathVar;
        cancerDeath = cancerDeathVar;
        healthyMutatesToCancer = healthyMutatesToCancerVar;
        cancerMutatesToStem = cancerMutatesToStemVar;
        cancerMutatesToFast = cancerMutatesToFastVar;
        healthyStemReplication = healthyStemReplicationVar;
        cancerStemReplication = cancerStemReplicationVar;
        cancerFastReplication = cancerFastReplicationVar;
        time = timeVar;
        deathTime = deathTimeVar;
        healthyNum = healthyNumVar;
        healthyStemNum = healthyStemNumVar;
        cancerNum = cancerNumVar;
        cancerStemNum = cancerStemNumVar;
        cancerFastNum = cancerFastNumVar;
        healthyResistance = healthyResistanceVar;
        healthyStemResistance = healthyStemResistanceVar;
        cancerResistance = cancerResistanceVar;
        cancerStemResistance = cancerStemResistanceVar;
        cancerFastResistance = cancerFastResistanceVar;
    }

    // Update is called once per frame
    void Update () {
        progress = GameObject.Find("ProgressBar").GetComponent<Slider>();
        setCountText();
        if (healthyAmount == 0)
        {
            SceneManager.LoadScene(2);
        }
        if (cancerAmount == 0)
        {
            SceneManager.LoadScene(3);
        }
        if (Input.GetKeyDown(KeyCode.Escape) && !isPause)
        {
            pauseMenu.SetActive(true);
            isPause = true;
            Time.timeScale = 0f;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isPause)
        {
            pauseMenu.SetActive(false);
            isPause = false;
            Time.timeScale = 1f;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.GetComponent<HealthyCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<HealthyCellReplication>().LoseLife());
                if (hit.collider.gameObject.GetComponent<HealthyStemCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<HealthyStemCellReplication>().LoseLife());
                if (hit.collider.gameObject.GetComponent<CancerStemCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<CancerStemCellReplication>().LoseLife());
                if (hit.collider.gameObject.GetComponent<FastCancerCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<FastCancerCellReplication>().LoseLife());
                if (hit.collider.gameObject.GetComponent<CancerCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<CancerCellReplication>().LoseLife());
            }
        }

    }
}
