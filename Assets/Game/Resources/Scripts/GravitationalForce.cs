﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitationalForce : MonoBehaviour {

    public float range = 30.0f;
    public float force = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Collider[] cols = Physics.OverlapSphere(transform.position, range);
        ArrayList rbs = new ArrayList();
        //List<Collider[]> cols = new List<Collider[]>();
        //cols = Physics.OverlapSphere(transform.position, range);
        //List<Collider> rbs = new List<Collider>();

        for (int c = 0; c < cols.Length; c++)
        {
            if (cols[c].attachedRigidbody && cols[c].attachedRigidbody != GetComponent<Rigidbody>())
            {
                bool breaking = false;
                for (int r = 0; r < rbs.Count; r++)
                {
                    if (cols[c].attachedRigidbody == (Object)rbs[r])
                    {
                        breaking = true;
                        break;
                    }
                }
                if (breaking) continue;
                rbs.Add(cols[c]);
                Vector3 offset = (transform.position - cols[c].transform.position);
                float mag = offset.magnitude;
                cols[c].attachedRigidbody.AddForce(offset / mag / mag * GetComponent<Rigidbody>().mass * force);
            }
        }
    }
}
