﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthyCellReplication : CellReplication
{

    public GameObject cancerCell;
    public int probMutation;
    public int probDeath;
    public int probIncreaseResistance;
    public float time;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitAndDuplicate());
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().maxAngularVelocity = 1;
    }

    override
    public IEnumerator WaitAndDuplicate()
    {
        yield return new WaitForSeconds(GameController.startTime);
        while (GameController.maxSpawn < GameController.max * 2)
        {
            probMutation = Random.Range(0, 100);
            probDeath = Random.Range(0, 100);
            probIncreaseResistance = Random.Range(0, 100);
            time = Random.Range(0, GameController.time);
            yield return new WaitForSeconds(time);

            Vector3 randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);

            if (probMutation < GameController.healthyMutatesToCancer && GameController.maxSpawn < GameController.max)
            {
                Instantiate(cancerCell, transform.position + randomOffset, transform.rotation);
                cancerCell.transform.Rotate(randomOffset, Time.deltaTime * 10);
                Destroy(gameObject);
                GameController.cancerAmount++;
            }
            else if (probDeath < GameController.healthyDeath && GameController.maxSpawn < GameController.max)
            {
                Destroy(gameObject);
                GameController.maxSpawn--;
            }
            else if (probIncreaseResistance < GameController.healthyResistance && GameController.maxSpawn < GameController.max)
            {
                life++;
            }
            yield return new WaitForSeconds(GameController.time - time);
        }
    }

    override
    public void Decrement()
    {
        GameController.maxSpawn--;
    }

    override
    public void DecrementTutorial()
    {
        TutorialController.maxSpawn--;
    }

    override
    public void Increment()
    {
        GameController.maxSpawn++;
        GameController.cancerAmount++;
    }

}
