﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastCancerCellReplication : CellReplication
{

    public GameObject cancerCell;
    int probReplication;
    int probIncreaseResistance;
    float time;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitAndDuplicate());
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().maxAngularVelocity = 1;
    }

    override
    public IEnumerator WaitAndDuplicate()
    {
        yield return new WaitForSeconds(GameController.startTime);
        while (GameController.maxSpawn < GameController.max * 2)
        {
            probReplication = Random.Range(0, 100);
            probIncreaseResistance = Random.Range(0, 100);
            time = Random.Range(0, GameController.time);
            yield return new WaitForSeconds(time);

            Vector3 randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            
            if (probReplication < GameController.cancerFastReplication && GameController.maxSpawn < GameController.max)
            {
                Instantiate(cancerCell, transform.position + randomOffset, transform.rotation);
                cancerCell.transform.Rotate(randomOffset, Time.deltaTime * 10);
                Increment();
            }
            else if (probIncreaseResistance < GameController.cancerFastResistance && GameController.maxSpawn < GameController.max)
            {
                life++;
            }
            yield return new WaitForSeconds(GameController.time - time);
        }
    }
}
