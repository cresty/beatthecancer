﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour {

    
    public static int maxSpawn = 0;
    public static int max = 100;
    public static GameObject[] Prefabs;
    public static float radio = 1.0f;
    public static float time;
    public static float startTime;
    public static float deathTime;
    public static int healthyDeath;
    public static int cancerDeath;
    public static int healthyMutatesToCancer;
    public static int cancerMutatesToStem;
    public static int cancerMutatesToFast;
    public static int healthyStemReplication;
    public static int cancerStemReplication;
    public static int cancerFastReplication;
    public static int healthyResistance;
    public static int healthyStemResistance;
    public static int cancerResistance;
    public static int cancerStemResistance;
    public static int cancerFastResistance;
    public static int healthyNum;
    public static int healthyStemNum;
    public static int cancerStemNum;
    public static int cancerFastNum;
    public static int cancerNum;

    
    public bool kill = false;
    public bool cont = false;
    public static int healthyAmount;
    public static int cancerAmount;
    public GameObject countCanvas;
    public bool isPause = false;
    public GameObject pauseMenu;
    public GameObject tutorialMenu;
    public Text tutorialText;
    public LanguageManager lang;
    public Vector3 randomOffset;
    public Slider progress;

    // Use this for initialization
    void Start () {
        healthyAmount = 0;
        cancerAmount = 0;
        GameController.time = 0;
        StartCoroutine(Tutorial());
    }
	
	// Update is called once per frame
	void setCountText () {
        progress.maxValue = healthyAmount + cancerAmount;
        progress.value = healthyAmount;
    }

    public IEnumerator Tutorial()
    {
        countCanvas.GetComponent<CanvasGroup>().alpha = 0;
        lang = LanguageManager.Load();
        randomOffset = Random.insideUnitCircle * GameController.radio;
        randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
        Prefabs = Resources.LoadAll<GameObject>("Prefabs") as GameObject[];
        Paneltutorial("tutext1");
        Instantiate(Prefabs[4], (randomOffset), Quaternion.identity);
        yield return new WaitForSeconds(2);
        Paneltutorial("tutext2");
        GameController.startTime = 1;
        GameController.time = 2;
        GameController.healthyStemReplication = 100;
        yield return new WaitForSeconds(2);
        GameController.time = 0;
        GameController.healthyStemReplication = 0;
        Paneltutorial("tutext3");
        yield return new WaitForSeconds(1);
        Destroy(GameObject.FindWithTag("MadreSana"));
        GameController.startTime = 3;
        yield return new WaitForSeconds(1);
        Paneltutorial("tutext4");
        GameController.startTime = 1;
        yield return new WaitForSeconds(1);
        Destroy(GameObject.FindWithTag("Sana"));
        yield return new WaitForSeconds(1);
        Paneltutorial("tutext5");
        GameController.healthyMutatesToCancer = 100;
        GameController.healthyDeath = 0;
        Instantiate(Prefabs[1], (randomOffset), Quaternion.identity);
        yield return new WaitForSeconds(2);
        cancerAmount++;
        Paneltutorial("tutext6");
        yield return new WaitForSeconds(2);
        Paneltutorial("tutext7");
        kill = true;
        while (cancerAmount != 0)
        {
            yield return new WaitForSeconds(0.1f);
        }
        Paneltutorial("tutext8");
        kill = false;
        Instantiate(Prefabs[0], (randomOffset), Quaternion.identity);
        cancerAmount++;
        yield return new WaitForSeconds(2);
        Paneltutorial("tutext9");
        GameController.startTime = 1;
        GameController.time = 2;
        GameController.cancerDeath = 0;
        GameController.cancerMutatesToStem = 100;
        yield return new WaitForSeconds(2);
        Paneltutorial("tutext10");
        GameController.cancerMutatesToStem = 0;
        GameController.cancerStemReplication = 100;
        yield return new WaitForSeconds(2);
        Paneltutorial("tutext11");
        GameController.cancerStemReplication = 0;
        yield return new WaitForSeconds(2);
        Destroy(GameObject.FindWithTag("Cancer"));
        cancerAmount--;
        Destroy(GameObject.FindWithTag("MadreCancer"));
        Paneltutorial("tutext12");
        yield return new WaitForSeconds(0.1f);
        Paneltutorial("tutext13");
        kill = true;
        healthyAmount = 0;
        cancerAmount = 0;
        GameController.Init(35, 55, 15, 20, 20, 50, 20, 20, 10, 0.0f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        for (int i = 0; i < 2; i++)              // Celulas Cancer
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[0], (randomOffset), Quaternion.identity);
            cancerAmount++;
        }
        for (int i = 0; i < 3; i++)               // Celulas Sanas
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[1], (randomOffset), Quaternion.identity);
        }
        for (int i = 0; i < 2; i++)         // Celulas Cancer Madre
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[2], (randomOffset), Quaternion.identity);
            cancerAmount++;
        }
        for (int i = 0; i < 3; i++)    //Celulas Madre Sanas
        {
            randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);
            Instantiate(Prefabs[4], (randomOffset), Quaternion.identity);
            healthyAmount++;
        }
        countCanvas.GetComponent<CanvasGroup>().alpha = 1;
        while (cancerAmount != 0 && healthyAmount != 0)
        {
            yield return new WaitForSeconds(0.1f);
        }
        if (healthyAmount == 0)
        {
            Paneltutorial("tutext14");
            while (isPause)
            {
                yield return new WaitForSeconds(0.1f);
            }
            SceneManager.LoadSceneAsync(2);
        }
        if (cancerAmount == 0)
        {
            Paneltutorial("tutext15");
            while (isPause)
            {
                yield return new WaitForSeconds(0.1f);
            }
            SceneManager.LoadSceneAsync(3);
        }
    }

    public void Paneltutorial(string texto)
    {
        tutorialText.text = lang.GetString(texto);
        tutorialMenu.SetActive(true);
        isPause = true;
        Time.timeScale = 0f;
    }

    static public void Init(int healthyDeathVar, int cancerDeathVar, int healthyMutatesToCancerVar, int cancerMutatesToStemVar,
        int cancerMutatesToFastVar, int healthyStemReplicationVar, int cancerStemReplicationVar, int cancerFastReplicationVar,
        float timeVar, float deathTimeVar, int healthyNumVar, int healthyStemNumVar, int cancerNumVar, int cancerStemNumVar,
        int cancerFastNumVar, int healthyResistanceVar, int healthyStemResistanceVar, int cancerResistanceVar, int cancerStemResistanceVar,
        int cancerFastResistanceVar)
    {
        /*
        * 0 Healthy Cell Dies
        * 1 Cancer Cell Dies
        * 2 Healthy Cell Mutates
        * 3 Cancer Cell Mutates into Cancer Stem Cell
        * 4 Cancer Cell Mutates en Fast Cancer Cell
        * 5 Healthy Stem Cell Replicates
        * 6 Cancer Stem Cell Replicates
        * 7 Fast Cancer Cell Replicates
        * 8 Time
        * 9 Delay for Cell Death
        * 19 Initial Healthy Cells
        * 11 Initial Healthy Stem Cells
        * 12 Initial Cancer Cells
        * 13 Initial Cancer Stem Cells
        * 14 Initial Fast Cancer Cells
        * 15 Healthy Cells Increase Resistance
        * 16 Healthy Stem Cells Increase Resistance
        * 17 Cancer Cells Increase Resistance
        * 18 Cancer Stem Cells Increase Resistance
        * 19 Fast Cancer Cells Increase Resistance
        */
        //Prefabs = Resources.LoadAll("Prefabs", GameObject);
        healthyDeath = healthyDeathVar;
        cancerDeath = cancerDeathVar;
        healthyMutatesToCancer = healthyMutatesToCancerVar;
        cancerMutatesToStem = cancerMutatesToStemVar;
        cancerMutatesToFast = cancerMutatesToFastVar;
        healthyStemReplication = healthyStemReplicationVar;
        cancerStemReplication = cancerStemReplicationVar;
        cancerFastReplication = cancerFastReplicationVar;
        time = timeVar;
        deathTime = deathTimeVar;
        healthyNum = healthyNumVar;
        healthyStemNum = healthyStemNumVar;
        cancerNum = cancerNumVar;
        cancerStemNum = cancerStemNumVar;
        cancerFastNum = cancerFastNumVar;
        healthyResistance = healthyResistanceVar;
        healthyStemResistance = healthyStemResistanceVar;
        cancerResistance = cancerResistanceVar;
        cancerStemResistance = cancerStemResistanceVar;
        cancerFastResistance = cancerFastResistanceVar;


    }

    // Update is called once per frame
    void Update()
    {
        progress = GameObject.Find("ProgressBar").GetComponent<Slider>();
        setCountText();
        if (Input.GetKeyDown(KeyCode.Escape) && !isPause)
        {
            pauseMenu.SetActive(true);
            isPause = true;
            Time.timeScale = 0f;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isPause)
        {
            pauseMenu.SetActive(false);
            isPause = false;
            Time.timeScale = 1f;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && kill)
            {
                if (hit.collider.gameObject.GetComponent<HealthyCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<HealthyCellReplication>().LoseLifeTutorial());
                if (hit.collider.gameObject.GetComponent<HealthyStemCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<HealthyStemCellReplication>().LoseLifeTutorial());
                if (hit.collider.gameObject.GetComponent<CancerStemCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<CancerStemCellReplication>().LoseLifeTutorial());
                if (hit.collider.gameObject.GetComponent<FastCancerCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<FastCancerCellReplication>().LoseLifeTutorial());
                if (hit.collider.gameObject.GetComponent<CancerCellReplication>())
                    StartCoroutine(hit.collider.gameObject.GetComponent<CancerCellReplication>().LoseLifeTutorial());
            }
        }

    }
}
