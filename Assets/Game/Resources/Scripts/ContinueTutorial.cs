﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ContinueTutorial : MonoBehaviour, IPointerClickHandler
{
    public TutorialController tutorialController;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnPointerClick(PointerEventData eventData)
    {
        tutorialController.tutorialMenu.SetActive(false);
        tutorialController.isPause = false;
        Time.timeScale = 1f;
    }
}
