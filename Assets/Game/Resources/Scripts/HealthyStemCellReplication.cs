﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthyStemCellReplication : CellReplication
{

    public GameObject healthyCell;
    int probReplication;
    int probIncreaseResistance;
    float time;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitAndDuplicate());
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().maxAngularVelocity = 1;
    }

    override
    public IEnumerator WaitAndDuplicate()
    {
        yield return new WaitForSeconds(GameController.startTime);
        while (GameController.maxSpawn < GameController.max * 2)
        {
            probReplication = Random.Range(0, 100);
            probIncreaseResistance = Random.Range(0, 100);
            time = Random.Range(0, GameController.time);
            yield return new WaitForSeconds(time);

            Vector3 randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);

            if (probReplication < GameController.healthyStemReplication && GameController.maxSpawn < GameController.max)
            {
                Instantiate(healthyCell, transform.position + randomOffset, transform.rotation);
                Increment();
            }
            else if (probIncreaseResistance < GameController.healthyStemResistance && GameController.maxSpawn < GameController.max)
            {
                life++;
            }
            yield return new WaitForSeconds(GameController.time - time);
        }
    }

    override
    public void Decrement()
    {
        GameController.maxSpawn--;
        GameController.healthyAmount--;
    }

    override
    public void DecrementTutorial()
    {
        TutorialController.maxSpawn--;
        TutorialController.healthyAmount--;
    }

    override
    public void Increment()
    {
        GameController.maxSpawn++;
    }

}
