﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Resume : MonoBehaviour, IPointerClickHandler
{
    public GameController gameController;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        gameController.pauseMenu.SetActive(false);
        gameController.isPause = false;
        Time.timeScale = 1f;
    }
}