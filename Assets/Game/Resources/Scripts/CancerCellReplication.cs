﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancerCellReplication : CellReplication {

    public GameObject cancerStemCell;
    public GameObject fastCancerCell;
    int probDeath;
    int probStemMutation;
    int probFastMutation;
    int probIncreaseResistance;
    float time;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitAndDuplicate());
    }

    // Update is called once per frame
    void Update() {
        GetComponent<Rigidbody>().maxAngularVelocity = 1;
    }

    override
    public IEnumerator WaitAndDuplicate()
    {
        yield return new WaitForSeconds(GameController.startTime);
        while (GameController.maxSpawn < GameController.max * 2)
        {
            probDeath = Random.Range(0, 100);
            probStemMutation = Random.Range(0, 100);
            probFastMutation = Random.Range(0, 100);
            probIncreaseResistance = Random.Range(0, 100);
            time = Random.Range(0, GameController.time);
            yield return new WaitForSeconds(time);

            Vector3 randomOffset = Random.insideUnitCircle * GameController.radio;
            randomOffset = new Vector3(randomOffset.x, randomOffset.z, randomOffset.y);

            if (probDeath < GameController.cancerDeath && GameController.maxSpawn < GameController.max)
            {
                Destroy(gameObject);
                Decrement();
            }
            else if (probStemMutation < GameController.cancerMutatesToStem && GameController.maxSpawn < GameController.max)
            {
                Instantiate(cancerStemCell, transform.position + randomOffset, transform.rotation);
                cancerStemCell.transform.Rotate(randomOffset, Time.deltaTime * 10);
                Destroy(gameObject);
            }
            else if (probFastMutation < GameController.cancerMutatesToFast && GameController.maxSpawn < GameController.max)
            {
                Instantiate(fastCancerCell, transform.position + randomOffset, transform.rotation);
                fastCancerCell.transform.Rotate(randomOffset, Time.deltaTime * 1);
                Destroy(gameObject);
            }
            else if (probIncreaseResistance < GameController.cancerResistance && GameController.maxSpawn < GameController.max)
            {
                life++;
            }
            yield return new WaitForSeconds(GameController.time - time);
        }
    }

}
